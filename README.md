# MajorLeagueNetworking
Group 18 - BOLT - Senior Software Project - ITCS 4155

Team Members: Serge Neri, Mosuela Joanne, Darshesh Patel, Michael Pedersen, Shilpa Arora

# Goal
Major Leage Networking (MLN) is a small talk aid that helps users immediately engage in conversation about sports.
Users query MLN in realtime for any sports topic and receive the most relevant talking points for use in small talk interactions.
MLN empowers a user's social life, breaking the ice with co-workers and new acquaintances.

# How to install
1. Open a terminal
2. Clone this repo

	  ```git clone https://baocin@bitbucket.org/ITCS4155-Bolt/majorleaguenetworking.git```
  
3. Navigate into the cloned directory

	   ```cd majorleaguenetworking```
   
4. Copy the "api_keys.json" file into this directory. It should look something like this:
   >
    {
       "Twitter":
        {
            "CONSUMER_KEY": "XXXXXXXXXXXXXXXXXXX",
            "CONSUMER_SECRET": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "ACCESS_TOKEN_KEY": "XXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "ACCESS_TOKEN_SECRET": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        }
    }
   > #### If you are a grader you can access the api_keys.json from [my google drive](https://drive.google.com/file/d/0B8bK4DhMreOTYkxUSkxpbldGRms/view?usp=sharing)
5. This command starts up all FOUR servers. The servers consume ports 3000,5000,9000, and 9090. To quit them all press enter.

	   ```./start_all.sh```

6. Open your favorite browser (Chrome) to http://localhost:3000 

  * ##### If the website doesn't display or return any tweets after querying("Cam Newton")  try the following:
	1. Make sure you have Python3 (https://www.python.org/) and Node.js (https://nodejs.org/en/download/) installed
    1. Close the terminal and open a new one to the clone directory

    2. Type 
    
	       ```npm install```
	       
	       ```npm start```
	       
    3. Open another terminal at the cloned directory

    4. Type
	
	       ```cd LanguageProcessingAPI```
	       
	       ```sudo pip3 install textstat```
	       
	       ```sudo pip3 install newspaper3k```
	       
	       ```sudo pip3 install flask```
	       
	       ```sudo pip3 install nltk```
	       
	       ```python3 run.py```

    5. Open another terminal in the Language Processing API Folder
	
	       ```cd SimpleNLG```
	       
	       ```./start.sh```

    6. Open another terminal in the Language Processing API Folder (cd ../)
	
	       ```cd stanford-corenlp-full-2016-10-31```
	       
	       ```./start.sh```
     
7. Use the website! ( I suggest "Cam Newton" as a search query)
8. ...
9. Profit?


# Technologies Used: 
- Node.js: UI/Presentation Layer
    - [webkitSpeechRecognition](https://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html): Client-side 
    - [pos](https://www.npmjs.com/package/pos): Client-side word classification into Noun, Verb, etc. 
    - [Bootstrap](http://getbootstrap.com/): Making everything look pretty
        - JQuery: Prebundled
    - [Notify.js](https://notifyjs.com/): Little alerts about voice recognition status
    - [Browserify](http://browserify.org/): To require() npm libraries client-side
    - [request](https://www.npmjs.com/package/request): Make REST API calls a whole lot easier
    - [express.js](http://expressjs.com/): Simple routing and http server
    - [twitter](https://www.npmjs.com/package/twitter): Needed inorder to interact with Twitter API
    - [EJS](https://www.npmjs.com/package/ejs): HTML Templating

- Python: News Article Extraction Service
    - [newspaper](http://newspaper.readthedocs.io/): Extract just the text out of a news article
    - [Flask](http://flask.pocoo.org/): Used to expose a REST API to other components of the system
    - [requests](http://docs.python-requests.org/en/master/): Made calling REST APIs well

- Java: [Standford CoreNLP Server](http://stanfordnlp.github.io/CoreNLP/corenlp-server.html)
    - Used to extract subject-relation-object triples from news article text

- Java: [SimpleNLG Service](https://github.com/simplenlg/simplenlg)
    - For generating sentences from extracted "fact" triples